package com.stefanini.core.ws.entity;

import java.io.Serializable;

/**
 * Representa la entidad con los atributos "codigo" y "valor" para la comunicación con servicio web externo.
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 */
public class DtoCodeValue implements Serializable {
	private static final long serialVersionUID = 1L;
	private String codigo;
	private int valor;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
}