package com.stefanini.core.ws.entity;

import java.io.Serializable;

/**
 * Representa la entidad de profesional y sus atributos para la comunicación con servicio web externo.
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 */
public class Professional implements Serializable {
	private static final long serialVersionUID = 1L;
	private String _id;
	private String nombres;
	private String locacion;
	private String cargo;
	private int experiencia;
	private String salario;
	private String disponibilidad;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getLocacion() {
		return locacion;
	}

	public void setLocacion(String locacion) {
		this.locacion = locacion;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public int getExperiencia() {
		return experiencia;
	}

	public void setExperiencia(int experiencia) {
		this.experiencia = experiencia;
	}

	public String getSalario() {
		return salario;
	}

	public void setSalario(String salario) {
		this.salario = salario;
	}

	public String getDisponibilidad() {
		return disponibilidad;
	}

	public void setDisponibilidad(String disponibilidad) {
		this.disponibilidad = disponibilidad;
	}
}
