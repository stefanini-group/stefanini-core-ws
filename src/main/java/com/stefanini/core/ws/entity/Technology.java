package com.stefanini.core.ws.entity;

import java.io.Serializable;

/**
 * Representa la entidad de tecnología y sus atributos para la comunicación con servicio web externo.
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 */
public class Technology implements Serializable {
	private static final long serialVersionUID = 1L;
	private String _id;
	private String codigo;
	private String nombre;
	private String imagen;
	private String clave[];
	private int valor;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String[] getClave() {
		return clave;
	}

	public void setClave(String[] clave) {
		this.clave = clave;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
}