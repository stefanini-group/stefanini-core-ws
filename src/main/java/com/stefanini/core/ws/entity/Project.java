package com.stefanini.core.ws.entity;

import java.io.Serializable;

/**
 * Representa la entidad de proyecto y sus atributos para la comunicación con servicio web externo.
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 */
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;
	private String _id;
	private String codigo;
	private String nombre;
	private String descripcion;
	private String imagen;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
}
