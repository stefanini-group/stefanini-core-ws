package com.stefanini.core.ws.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Representa la entidad de perfil y sus atributos para la comunicación con servicio web externo.
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 */
public class Profile implements Serializable {
	private static final long serialVersionUID = 1L;
	private String _id;
	private String nombres;
	private String identificacion;
	private String celular;
	private String corporativo;
	private String correo;
	private String locacion;
	private String imagen;
	private String sede;
	private String edad;
	private int vacaciones;
	private String otros;
	private String creditos;
	private String cargo;
	private String descripcion;
	private String estado;
	private String disponibilidad;
	private String asig[];
	private String roles[];
	private List<Technology> technologies;
	private List<Assignment> assignment;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorporativo() {
		return corporativo;
	}

	public void setCorporativo(String corporativo) {
		this.corporativo = corporativo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getLocacion() {
		return locacion;
	}

	public void setLocacion(String locacion) {
		this.locacion = locacion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public int getVacaciones() {
		return vacaciones;
	}

	public void setVacaciones(int vacaciones) {
		this.vacaciones = vacaciones;
	}

	public String getOtros() {
		return otros;
	}

	public void setOtros(String otros) {
		this.otros = otros;
	}

	public String getCreditos() {
		return creditos;
	}

	public void setCreditos(String creditos) {
		this.creditos = creditos;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDisponibilidad() {
		return disponibilidad;
	}

	public void setDisponibilidad(String disponibilidad) {
		this.disponibilidad = disponibilidad;
	}

	public String[] getAsig() {
		return asig;
	}

	public void setAsig(String[] asig) {
		this.asig = asig;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	public List<Technology> getTecnologias() {
		return technologies;
	}

	public void setTecnologias(List<Technology> technologies) {
		this.technologies = technologies;
	}

	public List<Assignment> getAsignacion() {
		return assignment;
	}

	public void setAsignacion(List<Assignment> assignment) {
		this.assignment = assignment;
	}
}
