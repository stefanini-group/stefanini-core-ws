package com.stefanini.core.ws.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.stefanini.core.ws.contract.IProjectsService;
import com.stefanini.core.ws.entity.Project;

/**
 * Define el objeto ProjectsService que contiene los métodos para consultar un
 * servicio web externo y sus operaciones GET, PUT, POST y DELETE
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 * @see <a href ="http://190.131.206.94:8091/group/artefactos/stefanini-core-ws"> Stefanini Group - Artefactos - Stefanini Core WS </a>
 */
public class ProjectsService implements IProjectsService {

	private ApiGateway apiGateway = new ApiGateway();
	private String userServiceUri = "usuario​/";
	private String userListServiceUri = userServiceUri + "lista";

	/**
	 * Devuelve listado de todos los proyectos disponibles.
	 * 
	 * @return Retorna listado de todos los proyectos disponibles.
	 */
	@Override
	public List<Project> getAllProjects() throws IOException {
		Map<String, String> parameters = new HashMap<>();
		List<Project> projectsList = new ArrayList<Project>();
		try {
			String responseService = apiGateway.serviceGetMethod(userListServiceUri, parameters);

			JsonObject response = new Gson().fromJson(responseService, JsonObject.class);
			JsonArray values = response.getAsJsonArray("payload");

			Type collectionType = new TypeToken<List<Project>>() {
			}.getType();
			projectsList = new Gson().fromJson(values, collectionType);
			return projectsList;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return projectsList;
		}
	}
}
