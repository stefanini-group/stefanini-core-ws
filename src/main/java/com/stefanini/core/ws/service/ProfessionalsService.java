package com.stefanini.core.ws.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.stefanini.core.ws.contract.IProfessionalsService;
import com.stefanini.core.ws.entity.Professional;

/**
 * Define el objeto ProfessionalsService que contiene los métodos para consultar
 * un servicio web externo y sus operaciones GET, PUT, POST y DELETE
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 * @see <a href = "http://190.131.206.94:8091/group/artefactos/stefanini-core-ws"> Stefanini Group - Artefactos - Stefanini Core WS </a>
 */
public class ProfessionalsService implements IProfessionalsService {

	private ApiGateway apiGateway = new ApiGateway();
	private String userServiceUri = "usuario​/";
	private String userListServiceUri = userServiceUri + "lista";

	/**
	 * Devuelve un listado de profesionales.
	 * 
	 * @return Retorna listado de profesionales.
	 */
	@Override
	public List<Professional> getAllProfessionals() throws IOException {
		Map<String, String> parameters = new HashMap<>();
		List<Professional> professionalsList = new ArrayList<Professional>();
		try {
			String responseService = apiGateway.serviceGetMethod(userListServiceUri, parameters);
			JsonObject response = new Gson().fromJson(responseService, JsonObject.class);
			JsonArray values = response.getAsJsonArray("payload");
			Type collectionType = new TypeToken<List<Professional>>() {
			}.getType();
			professionalsList = new Gson().fromJson(values, collectionType);
			return professionalsList;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return professionalsList;
		}
	}

	/**
	 * Devuelve el listado de profesionales que tienen asociado un nombre de
	 * tecnología.
	 * 
	 * @param tecnologyName Nombre de la tecnología.
	 * @return Retorna listado de profesionales asociados a un nombre de tecnología.
	 */
	@Override
	public List<Professional> getProfessionalsByTechnologyName(String tecnologyName) throws IOException {
		Map<String, String> parametros = new HashMap<>();
		List<Professional> professionalsList = new ArrayList<Professional>();
		try {
			parametros.put("tecnologia", tecnologyName);
			String responseService = apiGateway.serviceGetMethod(userListServiceUri, parametros);
			JsonObject response = new Gson().fromJson(responseService, JsonObject.class);
			JsonArray values = response.getAsJsonArray("payload");

			Type collectionType = new TypeToken<List<Professional>>() {
			}.getType();
			professionalsList = new Gson().fromJson(values, collectionType);
			return professionalsList;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return professionalsList;
		}
	}
}
