package com.stefanini.core.ws.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.stefanini.core.ws.contract.IAssignmentsService;
import com.stefanini.core.ws.entity.Assignment;
import com.stefanini.core.ws.entity.DtoCodeValue;
import com.stefanini.core.ws.entity.Profile;

/**
 * Define el objeto AssignmentsService que contiene los métodos para consultar
 * un servicio web externo y sus operaciones GET, PUT, POST y DELETE
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 * @see <a href = "http://190.131.206.94:8091/group/artefactos/stefanini-core-ws"> Stefanini Group - Artefactos - Stefanini Core WS </a>
 */
public class AssignmentsService implements IAssignmentsService {

	private ApiGateway apiGateway = new ApiGateway();
	private String userServiceUri = "usuario​/";
	private String profileServiceUri = userServiceUri + "perfil/";
	
	/**
     * Devuelve las asignaciones de un profesional.
     * @param professionalId Identificador del profesional.
     * @return Retorna listado de asignaciones del profesional.
     */
	@Override
	public List<Assignment> getAllAssignmentsByProfessionalId(String professionalId) throws IOException {
		Map<String, String> parameters = new HashMap<>();
		List<Assignment> listaAsignaciones = new ArrayList<Assignment>();
		try {
			parameters.put("id", professionalId);
			String responseService = apiGateway.serviceGetMethod(profileServiceUri, parameters);
			JsonObject response = new Gson().fromJson(responseService, JsonObject.class);
			JsonArray values = response.getAsJsonArray("payload");
			Type collectionType = new TypeToken<List<Profile>>() {
			}.getType();
			List<Profile> listaDatosDelPerfil = new Gson().fromJson(values, collectionType);
			listaAsignaciones = listaDatosDelPerfil.get(0).getAsignacion();
			return listaAsignaciones;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return listaAsignaciones;
		}
	}

	/**
     * Devuelve el resultado la creación de una asignación del profesional.
     * @param professionalId Identificador del profesional.
     * @return Retorna el resultado de crear la asignación del profesional.
     */
	@Override
	public String postAssignmentsByProfessionalId(String professionalId, DtoCodeValue dtoCodeValue) throws IOException {
		try {
			String uriExternalService = userServiceUri + professionalId + "/actualizar-asignacion";
			String bodyRequest = "[{\"codigo\":\"" + dtoCodeValue.getCodigo() + "\",\"valor\":"
					+ Integer.toString(dtoCodeValue.getValor()) + "}]";

			String responseService = apiGateway.servicePutMethod(uriExternalService, bodyRequest);

			return responseService;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return "";
		}
	}

	/**
     * Devuelve el resultado de modificar las asignaciones de un profesional.
     * @param professionalId Identificador del profesional.
     * @param dtoCodeValueList Listado de asignaciones "codigo":"valor"
     * @return Retorna el resultado de modificar las asignaciones del profesional.
     */
	@Override
	public String putAssignmentnByProfessionalId(String professionalId, List<DtoCodeValue> dtoCodeValueList)
			throws IOException {
		try {
			String bodyRequest = "[ ";
			for (DtoCodeValue dtoCodeValue : dtoCodeValueList) {
				bodyRequest.concat("[{\"codigo\":\"" + dtoCodeValue.getCodigo() + "\",\"valor\":"
						+ Integer.toString(dtoCodeValue.getValor()) + "}],");
			}
			bodyRequest = bodyRequest.substring(0, bodyRequest.length() - 1);
			bodyRequest = "]";

			String uriExternalService = userServiceUri + professionalId + "/actualizar-asignacion";

			String responseService = apiGateway.servicePutMethod(uriExternalService, bodyRequest);

			return responseService;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return "";
		}
	}

	/**
     * Devuelve el resultado de eliminar todas las asignaciones de un profesional.
     * @param professionalId Identificador del profesional.
     * @return Retorna el resultado de eliminar todas las asignaciones del profesional.
     */
	@Override
	public String deleteAllAssignmentsByProfessionalId(String professionalId) throws IOException {
		try {
			String bodyRequest = "[]";
			String uriExternalService = userServiceUri + professionalId + "/actualizar-asignacion";

			String responseService = apiGateway.servicePutMethod(uriExternalService, bodyRequest);

			return responseService;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return "";
		}
	}
}