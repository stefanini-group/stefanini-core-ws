package com.stefanini.core.ws.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.stefanini.core.ws.contract.IApiGateway;

/**
 * Define el objeto ApiGateway que contiene los métodos genéricos para conexión a servicios web externos y sus operaciones GET, PUT, POST y DELETE
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 * @see <a href = "http://190.131.206.94:8091/group/artefactos/stefanini-core-ws"> Stefanini Group - Artefactos - Stefanini Core WS </a>
 */
public class ApiGateway implements IApiGateway {

	private String externalServiceUri = "http://190.131.206.92:3000/squad/";
	
	/**
     * Devuelve el body con la decodificación del mensaje a partir del token en el parámetro.
     * @param token Define el token para decodificar.
     * @return Retorna el mensaje decodificado.
     */
	@Override
	public String decodeJwtMethod(String token) throws IOException {
		String[] split_string = token.split("\\.");
		String base64EncodedBody = split_string[1];
		Base64 base64Url = new Base64(true);
		String body = new String(base64Url.decode(base64EncodedBody));
		return body;
	}

	/**
     * Devuelve la respuesta al realizar una petición GET al servicio externo. 
     * @param url Define la ruta base del servicio externo.
     * @param parameters Define los parámetros que se envían al servicio externo.
     * @return Retorna la respuesta del servicio externo.
     */
	@Override
	public String serviceGetMethod(String url, Map<String, String> parameters) throws IOException {
		url = externalServiceUri + url;
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			for (Entry<String, String> entry : parameters.entrySet()) {
				params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}

			URIBuilder builder = new URIBuilder().setPath(url).setParameters(params);
			URI uri = builder.build();

			HttpGet httpGet = new HttpGet(uri);
			httpGet.addHeader("x-stf-auth", "1");
			httpGet.setHeader("Content-type", "application/json; charset=utf-8");

			ResponseHandler<String> responseHandler = response -> {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				}
				throw new ClientProtocolException("Unexpected response status: " + status);
			};

			String responseBody = httpClient.execute(httpGet, responseHandler);
			return responseBody;

		} catch (URISyntaxException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
     * Devuelve la respuesta al realizar una petición POST al servicio externo. 
     * @param url Define la ruta base del servicio externo.
     * @param body Define el cuerpo del mensaje (body) que se envía en la solicitud.
     * @return Retorna la respuesta del servicio externo.
     */
	@Override
	public String servicePostMethod(String url, String body) throws IOException {
		url = externalServiceUri + url;
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

			URIBuilder builder = new URIBuilder().setPath(url);
			URI uri = builder.build();

			HttpPost httpPost = new HttpPost(uri);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");

			StringEntity stringEntity = new StringEntity(body);
			httpPost.setEntity(stringEntity);

			ResponseHandler<String> responseHandler = response -> {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {

					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				}

				throw new ClientProtocolException("Unexpected response status: " + status);
			};

			String responseBody = httpClient.execute(httpPost, responseHandler);
			return responseBody;

		} catch (URISyntaxException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
     * Devuelve la respuesta al realizar una petición PUT al servicio externo. 
     * @param url Define la ruta base del servicio externo.
     * @param body Define el cuerpo del mensaje (body) que se envía en la solicitud.
     * @return Retorna la respuesta del servicio externo.
     */
	@Override
	public String servicePutMethod(String url, String body) throws IOException {
		url = externalServiceUri + url;
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

			URIBuilder builder = new URIBuilder().setPath(url);
			URI uri = builder.build();

			HttpPut httpPut = new HttpPut(uri);
			httpPut.addHeader("x-stf-auth", "1");
			httpPut.setHeader("Content-type", "application/json; charset=utf-8");

			StringEntity stringEntity = new StringEntity(body);
			httpPut.setEntity(stringEntity);

			ResponseHandler<String> responseHandler = response -> {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {

					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				}

				throw new ClientProtocolException("Unexpected response status: " + status);
			};

			String responseBody = httpClient.execute(httpPut, responseHandler);
			return responseBody;

		} catch (URISyntaxException e) {
			e.printStackTrace();
			return "";
		}
	}
}
