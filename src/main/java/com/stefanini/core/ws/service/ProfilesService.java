package com.stefanini.core.ws.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.stefanini.core.ws.contract.IProfilesService;
import com.stefanini.core.ws.entity.Profile;

/**
 * Define el objeto ProfilesService que contiene los métodos para consultar un
 * servicio web externo y sus operaciones GET, PUT, POST y DELETE
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 * @see <a href = "http://190.131.206.94:8091/group/artefactos/stefanini-core-ws"> Stefanini Group - Artefactos - Stefanini Core WS </a>
 */
public class ProfilesService implements IProfilesService {

	private ApiGateway apiGateway = new ApiGateway();
	private String userServiceUri = "usuario​/";
	private String profileServiceUri = userServiceUri + "perfil/";

	/**
	 * Devuelve listado de datos del perfil del profesional.
	 * 
	 * @param professionalId Identificador del profesional.
	 * @return Retorna listado de datos del perfil del profesional.
	 */
	@Override
	public List<Profile> getProfileByProfessionalId(String professionalId) throws IOException {
		Map<String, String> parametros = new HashMap<>();
		List<Profile> profileInformationList = new ArrayList<Profile>();
		try {

			parametros.put("id", professionalId);
			String responseService = apiGateway.serviceGetMethod(profileServiceUri, parametros);
			JsonObject response = new Gson().fromJson(responseService, JsonObject.class);
			JsonArray values = response.getAsJsonArray("payload");
			Type collectionType = new TypeToken<List<Profile>>() {
			}.getType();
			profileInformationList = new Gson().fromJson(values, collectionType);
			return profileInformationList;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return profileInformationList;
		}
	}
}
