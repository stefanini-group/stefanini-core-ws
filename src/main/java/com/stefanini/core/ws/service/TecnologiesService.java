package com.stefanini.core.ws.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.stefanini.core.ws.contract.ITechnologiesService;
import com.stefanini.core.ws.entity.DtoCodeValue;
import com.stefanini.core.ws.entity.Technology;

/**
 * Define el objeto TecnologiesService que contiene los métodos para consultar
 * un servicio web externo y sus operaciones GET, PUT, POST y DELETE
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 * @see <a href = "http://190.131.206.94:8091/group/artefactos/stefanini-core-ws"> Stefanini Group - Artefactos - Stefanini Core WS </a>
 */
public class TecnologiesService implements ITechnologiesService {

	private ApiGateway apiGateway = new ApiGateway();
	private String userServiceUri = "usuario​/";
	private String tecnologyServiceUri = "tecnologia​/";
	private String tecnologyListServiceUri = tecnologyServiceUri + "lista/";
	private String tecnologyOfUserServiceUri = tecnologyServiceUri + "tecnologias-user/";

	/**
	 * Devuelve listado de tecnologías.
	 * 
	 * @return Retorna listado de asignaciones del profesional.
	 */
	@Override
	public List<Technology> getAllTecnologies() throws IOException {
		Map<String, String> parameters = new HashMap<>();
		List<Technology> tecnologiesList = new ArrayList<Technology>();
		try {
			String responseService = apiGateway.serviceGetMethod(tecnologyListServiceUri, parameters);

			JsonObject response = new Gson().fromJson(responseService, JsonObject.class);
			JsonArray values = response.getAsJsonArray("payload");

			Type collectionType = new TypeToken<List<Technology>>() {
			}.getType();
			tecnologiesList = new Gson().fromJson(values, collectionType);

			return tecnologiesList;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return tecnologiesList;
		}
	}

	/**
	 * Devuelve las tecnologías asociadas al profesional.
	 * 
	 * @param professionalId Identificador del profesional.
	 * @return Retorna listado de tecnologías del profesional en la forma
	 *         "codigo":"valor".
	 */
	@Override
	public List<DtoCodeValue> getAllTecnologiesByProfessionalIdWithAttributesReduced(String professionalId)
			throws IOException {
		Map<String, String> parametros = new HashMap<>();
		List<DtoCodeValue> tecnologiesReducedList = new ArrayList<DtoCodeValue>();
		DtoCodeValue tecnologyReduced;
		try {
			parametros.put("id", professionalId);
			String responseService = apiGateway.serviceGetMethod(tecnologyOfUserServiceUri + professionalId, parametros);
			JsonObject response = new Gson().fromJson(responseService, JsonObject.class);
			JsonArray values = response.getAsJsonArray("payload");
			Type collectionType = new TypeToken<List<Technology>>() {
			}.getType();
			List<Technology> currentTecnologiesList = new Gson().fromJson(values, collectionType);
			for (Technology currentTecnology : currentTecnologiesList) {
				tecnologyReduced = new DtoCodeValue();
				tecnologyReduced.setCodigo(currentTecnology.getCodigo());
				tecnologyReduced.setValor(currentTecnology.getValor());
				tecnologiesReducedList.add(tecnologyReduced);
			}
			return tecnologiesReducedList;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return tecnologiesReducedList;
		}
	}

	/**
	 * Devuelve las tecnologías asociadas al profesional.
	 * 
	 * @param professionalId Identificador del profesional.
	 * @return Retorna listado de tecnologías del profesional.
	 */
	@Override
	public List<Technology> getAllTecnologiesByProfessionalId(String professionalId) throws IOException {
		Map<String, String> parameters = new HashMap<>();
		List<Technology> tecnologiesList = new ArrayList<Technology>();
		try {
			String responseService = apiGateway.serviceGetMethod(tecnologyOfUserServiceUri + professionalId, parameters);
			JsonObject response = new Gson().fromJson(responseService, JsonObject.class);
			JsonArray values = response.getAsJsonArray("payload");
			Type collectionType = new TypeToken<List<Technology>>() {
			}.getType();
			tecnologiesList = new Gson().fromJson(values, collectionType);
			return tecnologiesList;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return tecnologiesList;
		}
	}

	/**
	 * Devuelve el resultado de modificar las tecnologías asociadas al profesional.
	 * 
	 * @param professionalId             Identificador del profesional.
	 * @param listadoTecnologiasActuales Listado de tecnologías "codigo":"valor"
	 * @return Retorna el resultado de modificar las tecnologías del profesional.
	 */
	@Override
	public String putTecnologiesByProfessionalId(String professionalId, List<DtoCodeValue> listadoTecnologiasActuales)
			throws IOException {
		try {
			String bodyRequest = "[ ";
			for (DtoCodeValue item : listadoTecnologiasActuales) {
				bodyRequest.concat("[{\"codigo\":\"" + item.getCodigo() + "\",\"valor\":"
						+ Integer.toString(item.getValor()) + "}],");
			}
			bodyRequest = bodyRequest.substring(0, bodyRequest.length() - 1);
			bodyRequest = "]";
			String uriExternalService = userServiceUri + professionalId + "/actualizar-tecnologia";
			String responseService = apiGateway.servicePutMethod(uriExternalService, bodyRequest);
			return responseService;
		} catch (ClientProtocolException cpe) {
			cpe.printStackTrace();
			return "";
		}
	}
}