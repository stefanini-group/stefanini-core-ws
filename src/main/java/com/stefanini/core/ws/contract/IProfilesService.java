package com.stefanini.core.ws.contract;

import java.io.IOException;
import java.util.List;

import com.stefanini.core.ws.entity.Profile;

/**
 * Define el contrato o interface IProfilesService
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 */
public interface IProfilesService {

	List<Profile> getProfileByProfessionalId(String professionalId) throws IOException;
}