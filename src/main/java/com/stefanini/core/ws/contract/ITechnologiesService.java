package com.stefanini.core.ws.contract;

import java.io.IOException;
import java.util.List;

import com.stefanini.core.ws.entity.DtoCodeValue;
import com.stefanini.core.ws.entity.Technology;

/**
 * Define el contrato o interface ITechnologiesService
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 */
public interface ITechnologiesService {

	List<Technology> getAllTecnologies() throws IOException;

	List<Technology> getAllTecnologiesByProfessionalId(String professionalId) throws IOException;

	List<DtoCodeValue> getAllTecnologiesByProfessionalIdWithAttributesReduced(String professionalId) throws IOException;

	String putTecnologiesByProfessionalId(String professionalId, List<DtoCodeValue> currentListTechnologies)
			throws IOException;
}
