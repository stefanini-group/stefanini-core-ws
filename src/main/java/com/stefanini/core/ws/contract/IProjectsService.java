package com.stefanini.core.ws.contract;

import java.io.IOException;
import java.util.List;

import com.stefanini.core.ws.entity.Project;

/**
 * Define el contrato o interface IProjectsService
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 */
public interface IProjectsService {

	List<Project> getAllProjects() throws IOException;
}