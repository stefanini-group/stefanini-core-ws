package com.stefanini.core.ws.contract;

import java.io.IOException;
import java.util.List;

import com.stefanini.core.ws.entity.Assignment;
import com.stefanini.core.ws.entity.DtoCodeValue;

/**
 * Define el contrato o interface IAssignmentsService
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 */
public interface IAssignmentsService {

	List<Assignment> getAllAssignmentsByProfessionalId(String professionalId) throws IOException;

	String deleteAllAssignmentsByProfessionalId(String professionalId) throws IOException;

	String postAssignmentsByProfessionalId(String professionalId, DtoCodeValue dtoCodeValue) throws IOException;

	String putAssignmentnByProfessionalId(String professionalId, List<DtoCodeValue> dtoCodeValueList)
			throws IOException;
}