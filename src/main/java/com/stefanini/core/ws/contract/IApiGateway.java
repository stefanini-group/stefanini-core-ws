package com.stefanini.core.ws.contract;

import java.io.IOException;
import java.util.Map;

/**
 * Define el contrato o interface IApiGateway
 * 
 * @author: Gabriel Eduardo Duarte Vega
 * @version: 12/05/2020
 */
public interface IApiGateway {
	String decodeJwtMethod(String token) throws IOException;

	String serviceGetMethod(String url, Map<String, String> parameters) throws IOException;

	String servicePostMethod(String url, String body) throws IOException;

	String servicePutMethod(String url, String body) throws IOException;
}