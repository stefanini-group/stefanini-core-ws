package com.stefanini.core.ws.contract;

import java.io.IOException;
import java.util.List;

import com.stefanini.core.ws.entity.Professional;

/**
 * @author geduartev
 */
public interface IProfessionalsService {

	List<Professional> getAllProfessionals() throws IOException;

	List<Professional> getProfessionalsByTechnologyName(String tecnologyName) throws IOException;
}