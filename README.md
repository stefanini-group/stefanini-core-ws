#Introducción 
Stefanini Core WS is an API Library. API Library has layers of contracts, entities, and services.
These layers can connect with external web services to send requests and receive responses.
Coming soon, Stefanini Core WS will handle security with JWT Token. 

# Tareas iniciales
1.	El proceso de instalación
2.	Las dependencias de software
3.	Las últimas versiones
4.	Las referencias de API

# Compilación y prueba
Agregue en su proyecto Maven las siguientes dependencias:

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.2.6.RELEASE</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>
	<groupId>com.stefanini.webservice.springboot2</groupId>
	<artifactId>springboot2-maven-mssql-jpa-hibernate</artifactId>
	<version>1.0.0</version>
	<name>springboot2-maven-mssql-jpa-hibernate</name>
	<description>SpringBoot 2, Maven, MSSQLServer, JPA, Hibernate</description>
	<properties>
		<java.version>1.8</java.version>
	</properties>
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.junit.vintage</groupId>
					<artifactId>junit-vintage-engine</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>com.microsoft.sqlserver</groupId>
			<artifactId>mssql-jdbc</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>1.10</version>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>4.5.5</version>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpcore</artifactId>
			<version>4.4.9</version>
		</dependency>
		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>2.8.0</version>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

# Contribución
Pueden contribuir otros usuarios y desarrolladores a la mejora del código. 

Si quiere obtener más información sobre cómo crear archivos Léame descriptivos, consulte las siguientes [directrices](http://190.131.206.94:8091/group/artefactos/stefanini-core-ws/directrices).

# Informar problemas de seguridad y errores
Los problemas de seguridad y los errores se deben informar de forma privada, por correo electrónico, al Centro de respuesta de seguridad del Chapter Dev&Evolve adstechnicalmanager@stefanini.com
Debería recibir una respuesta dentro de las 24 horas. Si por alguna razón no lo hace, realice un seguimiento por correo electrónico para asegurarse de que recibimos su mensaje original.